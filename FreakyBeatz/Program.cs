﻿using DisCatSharp;
using DisCatSharp.ApplicationCommands;
using DisCatSharp.ApplicationCommands.Attributes;
using DisCatSharp.ApplicationCommands.Context;
using DisCatSharp.CommandsNext;
using DisCatSharp.Entities;
using DisCatSharp.Enums;
using DisCatSharp.Lavalink;
using DisCatSharp.Lavalink.Entities;
using DisCatSharp.Lavalink.Enums;
using DisCatSharp.Net;

namespace FreakyBeatz
{
    internal class Program
    {
        public static DiscordClient Discord { get; internal set; }
        static void Main(string[] args = null)
        {
            Console.WriteLine("Initialising FreakyBeatz");
            MainAsync().GetAwaiter().GetResult();
        }
        static async Task MainAsync()
        {
            // DO NOT OPEN ON STREAM
            var discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = "INSERT TOKEN HERE",
                TokenType = TokenType.Bot,
                Intents = DiscordIntents.AllUnprivileged | DiscordIntents.MessageContent
            });
            var endpoint = new ConnectionEndpoint
            {
                Hostname = "LavaLink IP",
                Port = 2333
            };
            var lavalinkConfig = new LavalinkConfiguration
            {
                Password = "LavaLink Password",
                RestEndpoint = endpoint,
                SocketEndpoint = endpoint
            };

            discord.MessageCreated += async (s, e) =>
            {
                if (e.Message.Content.ToLower().StartsWith("!!ping"))
                {
                    await e.Message.RespondAsync("Pong!");
                }
            };
            var lavalink = discord.UseLavalink();
            var appCommands = discord.UseApplicationCommands();


            await discord.ConnectAsync();
            await lavalink.ConnectAsync(lavalinkConfig);
            appCommands.RegisterGlobalCommands<FBCommands>();
            await Task.Delay(-1);
        }
    }
    public class FBCommands : ApplicationCommandsModule
    {
        [SlashCommandGroup("Util", "All Commands")]
        public class FBUtilsCommandGroup : ApplicationCommandsModule
        {
            [SlashCommand("Ping","Pong")]
            public async Task FBPing(InteractionContext ctx)
            {
                await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource, new DisCatSharp.Entities.DiscordInteractionResponseBuilder()
                {
                    Content = "Pong"
                });
            }
        }
        [SlashCommandGroup("Media", "Controls audio playback")]
        public class FBMediaCommandGroup : ApplicationCommandsModule
        {
            [SlashCommand("play", "Play a track form a youtube or youtube music link.")]
            public async Task PlayAsync(InteractionContext ctx, [Option("query", "The query to search for")] string query)
            {
                await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
                if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You are not in a voice channel."));
                    return;
                }

                var lavalink = ctx.Client.GetLavalink();
                var guildPlayer = lavalink.GetGuildPlayer(ctx.Guild);

                if (guildPlayer == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Lavalink is not connected."));
                    return;
                }

                var loadResult = await guildPlayer.LoadTracksAsync(LavalinkSearchType.Youtube, query);

                if (loadResult.LoadType == LavalinkLoadResultType.Empty || loadResult.LoadType == LavalinkLoadResultType.Error)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Track search failed for {query}."));
                    return;
                }

                LavalinkTrack track = loadResult.LoadType switch
                {
                    LavalinkLoadResultType.Track => loadResult.GetResultAs<LavalinkTrack>(),
                    LavalinkLoadResultType.Playlist => loadResult.GetResultAs<LavalinkPlaylist>().Tracks.First(),
                    LavalinkLoadResultType.Search => loadResult.GetResultAs<List<LavalinkTrack>>().First(),
                    _ => throw new InvalidOperationException("Unexpected load result type.")
                };

                await guildPlayer.PlayAsync(track);

                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Now playing {query}!"));
            }
            [SlashCommand("pause", "Pause a track")]
            public async Task PauseAsync(InteractionContext ctx)
            {
                await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
                if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You are not in a voice channel."));
                    return;
                }

                var lavalink = ctx.Client.GetLavalink();
                var guildPlayer = lavalink.GetGuildPlayer(ctx.Guild);

                if (guildPlayer == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Lavalink is not connected."));
                    return;
                }

                if (guildPlayer.CurrentTrack == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("There are no tracks loaded."));
                    return;
                }

                await guildPlayer.PauseAsync();
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Playback paused!"));
            }

            [SlashCommand("resume", "Resume a track")]
            public async Task ResumeAsync(InteractionContext ctx)
            {
                await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
                if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("You are not in a voice channel."));
                    return;
                }

                var lavalink = ctx.Client.GetLavalink();
                var guildPlayer = lavalink.GetGuildPlayer(ctx.Guild);

                if (guildPlayer == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Lavalink is not connected."));
                    return;
                }

                if (guildPlayer.CurrentTrack == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("There are no tracks loaded."));
                    return;
                }

                await guildPlayer.ResumeAsync();
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Playback resumed!"));
            }


        }
        [SlashCommandGroup("Channel", "Controls where the bot is")]
        public class FBVoiceCommandGroup : ApplicationCommandsModule
        {
            [SlashCommand("join", "Join a voice channel")]
            public async Task JoinAsync(InteractionContext ctx, [Option("channel", "Channel to joing")] DiscordChannel channel)
            {
                await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
                var lavalink = ctx.Client.GetLavalink();
                if (!lavalink.ConnectedSessions.Any())
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The Lavalink connection is not established"));
                    return;
                }

                var session = lavalink.ConnectedSessions.Values.First();

                var ctype = channel.Type.ToString();
                if (ctype != "Voice")
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Not a valid voice channel."));
                    Console.WriteLine(channel.Type.ToString());
                    return;
                }

                await session.ConnectAsync(channel);
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Joined {channel.Mention}!"));
            }

            [SlashCommand("leave", "Leave the currently connected channel")]
            public async Task LeaveAsync(InteractionContext ctx)
            {
                await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
                var lavalink = ctx.Client.GetLavalink();
                var guildPlayer = lavalink.GetGuildPlayer(ctx.Guild);
                if (guildPlayer == null)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Lavalink not connected."));
                    return;
                }

                await guildPlayer.DisconnectAsync();
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent($"Left {guildPlayer.Channel.Mention}!"));
            }
        }
    }
}
